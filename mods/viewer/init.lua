
editor_main = [[
size[20,12;]
box[-0.3,-0.3;13,12.6;#FFFFFF00]

label[13,0;Formspec Viewer 1.0]
textarea[13.3,1;7,12;editor;Formspec source code:;%s]
button[13,12;7,0;submit;Submit]
]]
dummy_fs = minetest.formspec_escape([[
size[8,8]
image[1,0.6;1,2;player.png]
list[current_player;main;0,3.5;8,4;]
list[current_player;craft;3,0;3,3;]
list[current_player;craftpreview;7,1;1,1;]
]])

minetest.register_on_joinplayer(function(player)
	minetest.show_formspec(player:get_player_name(), "EDITOR", editor_main:format(dummy_fs))
end)

minetest.register_on_player_receive_fields(function(player, formname, fields)
if fields.quit then
	minetest.request_shutdown()
end
local pattern = "size%[(%d*%.*%d*),(%d*%.*%d*)%]"
if formname == "EDITOR" and fields.editor then

		local w,h = fields.editor:match(pattern)
		w = tonumber(w) or 0
		h = tonumber(h) or 0
		if(w == 0 or h==0) then
			minetest.chat_send_all("Warning: size[w,h] does not exist or is not properly configured.")
		end

		local fs = fields.editor:gsub(pattern,string.format("box[-0.3,-0.3;%.2f,%.f;#000000]",w+0.6,h+0.6))
		minetest.chat_send_all(fs);

		minetest.show_formspec(player:get_player_name(), "EDITOR", editor_main:format(minetest.formspec_escape(fields.editor))..fs)
	end

end)